# Toy Swift Neural Network [![Build Status](https://travis-ci.org/pbardea/swiftneuralnet.svg?branch=master)](https://travis-ci.org/pbardea/swiftneuralnet)

I made a simple neural network strongly based off [this blog post](https://iamtrask.github.io/2015/07/12/basic-python-network/).

To run, run the NNTests class in NNTests.swift.
